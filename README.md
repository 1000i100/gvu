## [Demo en ligne](//gvu.g1.1000i100.fr)
# Gvu

<i>Anciennement : **Duniter-Network-View** puis **Duniter-Mycelia-View**</i>

<img src="https://git.duniter.org/uploads/-/system/project/avatar/63/gvu.png" alt="Logo Gvu" height="200px" />

Visualisation graphique de crypto-monnaies.
- Outil de visualisation : [Mycelia](//mycelia.tools)
- Crypto-monnaie visualisée : [Ğ1](//duniter.org/fr/wiki/licence-g1/)
- Sources de données :
  - blockchain et piscine via la base sqlite `duniter.db`
  - données cesium+ en requêtant la base elastic-search de duniter4j
  - offres et demandes en requêtant gchange et gannonce
  - calculs effectués durant le script d'import.
  
## Installation

### Prerequis
- obligatoire : [nodejs](https://nodejs.org/fr/)
- recommandé : [git](https://git-scm.com/)
- recommandé : [noeud duniter](https://duniter.org/fr/)

### Minimal
- télécharger [Gvu](https://git.duniter.org/clients/gvu) en [zip](https://git.duniter.org/clients/gvu/repository/master/archive.zip) ou avec la commande `git clone https://git.duniter.org/clients/gvu.git`
- rendez-vous dans le dossier du projet en ligne de commande.
- `npm install`
- `npm start`

Après quelques instant, votre navigateur s'ouvre avec votre visualisation de la Ğ1

### Actualiser manuellement
`npm run update`

### Actualiser automatiquement
- editer une tache cron avec la commande `crontab -e`
- insérer l'appel du script de mise à jour à la periodicité souhaitée :
  - 1 fois par jour : `0 0 * * * cd /dossierGvu/ && npm run update && npm run build`
  - ou 1 fois toutes les 5 minutes : `*/5 * * * * cd /dossierGvu/ && npm run update && npm run build`
- sauvegarder est quitter

### Rendre accessible en ligne
Configurer votre serveur web (apache, nginx ou autre) pour servir les fichier statique du sous-dossier `generated.build.public` de votre projet.


## Configuration

- Pour changer les sources de données par défaut, créer un fichier `cron.config.yml` inspiré de `cron.config.default.yml`.
- Pour changer les filtres de la colonne de gauche editer le fichier `public/config.yml`.
- Pour ajouter/changer les picto associés aux calques/filtres/légende rendez-vous dans `public/layers/` et placez vos picto en svg au nom du calque correspondant.
- Pour changer les les couleurs des différent élément de la visualisation, rendez vous dans `public/customStyle.styl`.
- Pour collecter d'autres données, editer le fichier `cron.js` et écrivez-y le code nécessaire en vous inspirant de l'existant.

## Contribuer au code
- créez un compte sur [git.duniter.org](https://git.duniter.org/users/sign_in) ou [framagit](https://framagit.org/users/sign_in).
- [créez un fork de Gvu à votre nom](https://git.duniter.org/clients/gvu/forks/new).
- cloner votre fork en local.
- faite évoluer le code
- git commit, git push
- vérifier que la CI passe.
- merge request sur gitlab.
- Merci !

## Contribuer autrement
- créez un compte sur [git.duniter.org](https://git.duniter.org/users/sign_in) ou [framagit](https://framagit.org/users/sign_in).
- Rendez-vous sur la page des [issues du projet](https://git.duniter.org/clients/gvu/issues)
- Pour tout bug ou suggestions, consultez les issues existantes pour vérifier que ça n'y est pas déjà, commentez au besoin, ajoutez de nouvelles issues le cas échéant.

Enfin, vous pouvez aussi contribuer financièrement :
- en Ğ1 : [👤 : 1000i100 🔑 : 2sZF6j2P](https://cesium.g1.1000i100.fr/#/app/wot/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT/1000i100)
- en monnaie dette : [don ponctuel](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6JAUDKLT5RBVE) ou [don récurant](https://liberapay.com/1000i100/)
