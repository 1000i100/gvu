const fs = require('fs-extra');
const Sqlite = require('better-sqlite3');
const json2csv = require('json2csv');

async function run(){
	await sql2files('DU', `select distinct
		null as daySinceLaunch,
		number as blockNumber,
		strftime('%d/%m/%Y', datetime(time, 'unixepoch')) as date,
		strftime('%H:%M:%S', datetime(time, 'unixepoch')) as horaire,
		membersCount
	from block
	where dividend is not null
	group by blockNumber
	order by blockNumber asc`);
	await sql2files('00h', `select distinct
		null as daySinceLaunch,
		number as blockNumber,
		strftime('%d/%m/%Y', datetime(time, 'unixepoch')) as date,
		strftime('%H:%M:%S', datetime(time, 'unixepoch')) as horaire,
		membersCount
	from block
	group by date
	having min(rowid)
	order by blockNumber asc`);
}
run();
async function sql2files(flavor, query){
	if(!sql2files.duniterDB) sql2files.duniterDB = new Sqlite('duniter.db',{readonly:true});
	const results = await sql2files.duniterDB.prepare(query).all();
	for(let i=0;i<results.length;i++){
		const r = results[i];
		r.daySinceLaunch = i;
		r.diff = r.membersCount - (i?results[i-1].membersCount:0);
	}
	await fs.writeFile(`./generated.build.public/memberCountEvolution.flavor-${flavor}.json`, JSON.stringify(results));
	await fs.writeFile(`./generated.build.public/memberCountEvolution.flavor-${flavor}.csv`, json2csv({data:results,fields:Object.keys(results[0])}));
	await fs.writeFile(`./generated.build.public/memberCountEvolution.flavor-${flavor}.html`, json2html(results));
}
function json2html(json){
	return `
<style>th, td {padding:0 10px; text-align: right;}</style>
<table>
<thead>
<tr><th>${Object.keys(json[0]).join('</th><th>')}</th></tr>
</thead>
<tbody>
${json.map(e=>`<tr>${Object.keys(e).map(k=>`<td>${e[k]}</td>`).join('')}</tr>`).join('')}
</tbody>
</table>`
}