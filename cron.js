// https://github.com/duniter/duniter4j/blob/master/doc/API.md#contents
const http = require('http');
const https = require('https');
const os = require('os');
const process = require('process');
const approx = require('filesize');
const fs = require('fs-extra');
const got = require('got');
const yaml = require('js-yaml');
const moment = require('moment');
const Sqlite = require('better-sqlite3');
moment.locale("fr");

const dataSources = {};
async function run(){
    await init();
    const graph = {node:{},link:{},memberCount:0,};
    const duniterDB = new Sqlite(dataSources.duniterDbPath,{readonly:true});

    async function sql2graph(sql,extraFunc){
        const raw = await duniterDB.prepare(sql).all();
        for(let elem of raw){
            if(elem.pub){ // elem = node
                mustExistNode(elem.pub,graph);
                for(let prop in elem) if(prop !== "pub") graph.node[elem.pub][prop] = elem[prop];
                if(extraFunc) extraFunc(graph.node[elem.pub],elem);
            }
            else if(elem.source && elem.target && elem.type){
                const linkId = unikLinkId(elem.type,elem.source,elem.target);
                mustExistLink(linkId,graph);
                for(let prop in elem) if(prop !== "source" && prop !== "target") graph.link[linkId][prop] = elem[prop];
                if(extraFunc) extraFunc(graph.link[linkId],elem);
            } else console.log("Erreur, type d'élément non reconnu : ",elem);
        }
        return raw.length;
    }
    console.log(`${await sql2graph('SELECT uid as pseudo, uid as label, pub FROM i_index')} pseudo imported.`);
    graph.memberCount = await sql2graph('SELECT pub, "true" as member FROM m_index WHERE expired_on is null and revoked_on is null')
    console.log(`${graph.memberCount} active members imported.`);
    console.log(`${await sql2graph('SELECT pub, "true" as revoked FROM m_index WHERE revoked_on is not null')} revoked accounts imported.`);
    console.log(`${await sql2graph('SELECT replace(substr(conditions,5),")","") as pub, (balance/100) as money FROM wallet')} monetary balance imported.`);
    console.log(`${await sql2graph('SELECT issuer as pub, count(time) as blockForged FROM block GROUP BY issuer')} block forgers imported.`);
    console.log(`${await sql2graph(`SELECT issuer as pub, count(time) as last30jBlockForged FROM block WHERE time > ${Date.now()/1000-24*3600*30} GROUP BY issuer`)} block forgers in last 30 days imported.`);
    console.log(`${await sql2graph(`SELECT issuer as pub, count(time) as last7jBlockForged FROM block WHERE time > ${Date.now()/1000-24*3600*7} GROUP BY issuer`)} block forgers in last 7 days imported.`);
    console.log(`${await sql2graph(`SELECT issuer as pub, count(time) as last24hBlockForged FROM block WHERE time > ${Date.now()/1000-24*3600} GROUP BY issuer`)} block forgers in last 24 hours imported.`);
    console.log(`${await sql2graph('SELECT `from` as source, `to` as target, "pending" as type, "certification en attente" as label, `expires_on` as expiration FROM `certifications_pending`',(link,elem)=>link.expiration = moment(elem.expiration,"X").calendar() )} pending certifications imported.`);
    console.log(`${await sql2graph('SELECT issuer as source, receiver as target, "cert" as type, "certification validé" as label, `expires_on` as expiration FROM c_index',(link,elem)=>link.expiration = moment(elem.expiration,"X").calendar() )} valid certifications imported.`);

    const data4 = await elastic2json('/g1/movement/_search?_source=issuer,recipient,amount,comment,unitbase');
    for(let elem of data4){
        if(elem._source.unitbase) console.err('unitbase not handled');
        const linkId = unikLinkId('tx',elem._source.issuer,elem._source.recipient);
        mustExistLink(linkId,graph);
        graph.link[linkId].label = 'transaction';

        if(!graph.link[linkId].amount) graph.link[linkId].amount = parseFloat(elem._source.amount)/100;
        else graph.link[linkId].amount += parseFloat(elem._source.amount)/100;
        graph.link[linkId].value = graph.link[linkId].amount;
    }
    const data1 = await elastic2json('/user/profile/_search?_source=title,description,city');
    for(let elem of data1){
        mustExistNode(elem._id,graph);
        if(elem._source.title) graph.node[elem._id].label = elem._source.title;
        if(elem._source.title) graph.node[elem._id].title = elem._source.title;
        if(elem._source.city) graph.node[elem._id].city = elem._source.city;
        if(elem._source.description) graph.node[elem._id].description = elem._source.description;
    }
    mapLink(graph);
    certCount(graph);
    distAll(graph);
    //distMax(true);
    deleteNodeRefToLinks(graph);

    const myceliaGraph = struct2array(graph);
    await writeData(myceliaGraph);
    console.log(`data regenerated : ${myceliaGraph.node.length} nodes and ${myceliaGraph.link.length} links`);
}
run();
// https://g1.data.duniter.fr/user/event/_search?size=1000&pretty&q=code:CERT_SENT%20OR%20code:CERT_RECEIVED%20OR%20code:TX_SENT%20OR%20code:TX_RECEIVED%20OR%20code:MEMBER_JOIN

async function init(){
    console.log("load default sources config");
    Object.assign(dataSources,yaml.safeLoad(await fs.readFile(`cron.config.default.yml`, 'utf8')));
    if(fs.existsSync("cron.config.yml")){
        console.log("merge/overwrite default with custom sources config");
        Object.assign(dataSources,yaml.safeLoad(await fs.readFile(`cron.config.yml`, 'utf8')));
    }
    else console.log("no custom sources config, skipping");

    dataSources.duniterDbPath = dataSources.duniterDbPath.replace(/~/g,os.homedir);
    if(!fs.existsSync(dataSources.duniterDbPath)){
        console.log(`${dataSources.duniterDbPath} not found, looking for local copy...`);
        if(!fs.existsSync("duniter.db")){
            console.log(`duniter.db not found, failover ${dataSources.duniterDbUrlFailover} online copy. Downloading...`);
            await wget(dataSources.duniterDbUrlFailover,'duniter.db');
            console.log("Done. duniter.db local copy saved.");
        }
        dataSources.duniterDbPath = "duniter.db";
    }
}
function wget(sourceUrl,targetPathFile){
    return new Promise( (resolve, reject)=>{
        got.stream(sourceUrl).pipe(fs.createWriteStream(targetPathFile))
            .on('end',resolve).on('finish',resolve).on('error', reject);
    });
}
function url2jsonObj(options = {}) {
    return new Promise( (resolve, reject)=>{
        let req = (options.port===443?https:http).request(options, (response) => {
            let strJson = '';
            response.on('data', (chunk)=> strJson+=chunk );
            response.on('end', function () {
                resolve(JSON.parse(strJson));
            });
        });
        if(options.postData) req.write(options.postData);
        req.end();
    });
}
async function elastic2json(query) {
    const res = [];
    let offset = 0;
    while(1){
        const raw = await url2jsonObj({
            host: dataSources.duniter4jUrl,
            path: `${query}&from=${offset}&size=1000`
        });
        raw.hits.hits.forEach(e=>res.push(e));
        offset+=1000;
        if(raw.hits.total<offset) break;
    }
    return res;
}
function struct2array(struct) {
    const myceliaGraph = {node:[],link:[]};
    for(let i in struct.node) myceliaGraph.node.push(struct.node[i]);
    for(let i in struct.link) myceliaGraph.link.push(struct.link[i]);
    return myceliaGraph;
}
function distAll(graph) {
    console.log(`node minimal distances from any other node calculation...`);
    for(let i in graph.node) {
        graph.node[i].dist={};
    }
    for(let i in graph.link){
        const l = graph.link[i];
        if(l.type === 'cert'){
            graph.node[l.target.substr(5)].dist[l.source.substr(5)] = 1;
        }
    }
    let changed = 0;
    let step = 1;
    do{
        console.log(`distance ${step} mapped using ${approx(process.memoryUsage().rss)} ram. Available memory : ${approx(os.freemem())}/${approx(os.totalmem())}`);
        step++;
        changed = 0;
        for(let i in graph.link){
            const l = graph.link[i];
            if(l.type === 'cert'){
                const srcDists = graph.node[l.source.substr(5)].dist;
                const targetDists = graph.node[l.target.substr(5)].dist;
                for(let pub in srcDists){
                    const dist = srcDists[pub]+1
                    if(!targetDists[pub] || targetDists[pub] > dist){
                        targetDists[pub] = dist;
                        changed++;
                    }
                }
            }
        }
    } while (changed>0);
    console.log(`node minimal distances from any other node stored. (using ${approx(process.memoryUsage().rss)} ram. Available memory : ${approx(os.freemem())}/${approx(os.totalmem())})`);


    const memberCount = graph.memberCount;
    const referCount = graph.referPub.length;
    const infini = `unreachable`;
    function memberDist(member){
        member.incomingDistanceFromMembers = distTab(member.dist);
        member.incomingDistanceFromMembers[infini] = memberCount - Object.keys(member.dist).length;

        const memberDistFromRef = referFilter(member.dist,graph.referPub);
        member.incomingDistanceFromRefers = distTab(memberDistFromRef);
        member.incomingDistanceFromRefers[infini] = referCount - Object.keys(memberDistFromRef).length;

        member.farestRefers = farestRefers(member.dist,graph.referPub);

        const max5Dist = Object.keys(member.dist).filter(pub=>member.dist[pub]<=5);
        const max5DistFromRef = Object.keys(memberDistFromRef).filter(pub=>memberDistFromRef[pub]<=5);

        member.validDistFromMembers = `${Math.round(1000*max5Dist.length/memberCount)/10}%`;
        member.validDistFromRefers = `${Math.round(1000*max5DistFromRef.length/referCount)/10}%`;
    }
    function distTab(distList){
        const res = {};
        for(let pub in distList) res[distList[pub]]?res[distList[pub]]++:res[distList[pub]]=1;
        return res;
    }

    for(let pub of graph.membersPub){
        memberDist(graph.node[pub]);
    }
}
function referFilter(distList,referList){
    const filtered = {};
    for(let pub in distList) if(referList.indexOf(pub)!== -1) filtered[pub] = distList[pub];
    return filtered;
}
function farestRefers(distList,referList){
    const refDist = referFilter(distList,referList);
    if(Object.keys(refDist).length<referList.length) return referList.filter(pub=>!refDist[pub]);
    let maxDist = 0;
    for(let pub in refDist) if(refDist[pub]>maxDist){
        maxDist = refDist[pub];
    }
    return referList.filter(pub=>!refDist[pub]);
}
function mapLink(graph){
    for(let i in graph.node) {
        graph.node[i].startingLinks=[];
        graph.node[i].endingLinks=[];
    }
    for(let i in graph.link){
        const l = graph.link[i];
        graph.node[l.source.substr(5)].startingLinks.push(l);
        graph.node[l.target.substr(5)].endingLinks.push(l);
    }
}
function deleteNodeRefToLinks(graph){
    for(let i in graph.node) {
        delete graph.node[i].startingLinks;
        delete graph.node[i].endingLinks;
        delete graph.node[i].dist;
    }
}
function certCount(graph){
    const distanceMaxG1 = 5;
    const referMemberCondition = Math.ceil(Math.pow(graph.memberCount,1 / distanceMaxG1));
    for(let i in graph.node) {
        const currentNode = graph.node[i];
        currentNode.validSendedCert = currentNode.startingLinks.filter(l=>l.type === 'cert').length;
        currentNode.validReveivededCert = currentNode.endingLinks.filter(l=>l.type === 'cert').length;
        const referLevel = Math.min(currentNode.validSendedCert,currentNode.validReveivededCert);
        if(referLevel>=referMemberCondition) currentNode.referMember = referLevel;
    }
    graph.membersPub = Object.keys(graph.node).filter(pub=>graph.node[pub].member);
    graph.referPub = Object.keys(graph.node).filter(pub=>graph.node[pub].referMember);
    //graph.referPubNext1 = Object.keys(graph.node).filter(pub=>graph.node[pub].referMember>=1+referMemberCondition);
    //graph.referPubNext2 = Object.keys(graph.node).filter(pub=>graph.node[pub].referMember>=2+referMemberCondition);
    //graph.referPubNext3 = Object.keys(graph.node).filter(pub=>graph.node[pub].referMember>=3+referMemberCondition);
    console.log(`${graph.referPub.length} referent members calculated.`)
}
async function writeData(data){
    return await fs.writeFile('data.yml', yaml.safeDump(data));
}
function mustExistNode(id,graph){
    if(!graph.node[id]){
        graph.node[id] = {id:'node-'+id};
        graph.node[id].publicKey = id;
        graph.node[id].label = id.substr(0,8);
        graph.node[id].money = 0;
    }
}
function unikLinkId(type,source,target){return 'link-'+type+'-'+source+'-'+target;}
function mustExistLink(id,graph){
    if(!graph.link[id]){
        graph.link[id] = {"id":id};
        const idPart = id.split('-');
        graph.link[id].type = idPart[1];
        graph.link[id].source = 'node-'+idPart[2];
        graph.link[id].target = 'node-'+idPart[3];
        mustExistNode(idPart[2],graph);
        mustExistNode(idPart[3],graph)
    }
}
